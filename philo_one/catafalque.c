/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   catafalque.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 23:16:06 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/16 23:16:07 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_one.h"
#include "utils.h"

int		catafalque(t_philo **philos, t_traits *traits)
{
	int	i;

	while (!g_table->death)
	{
		i = 0;
		while (i < g_table->philo_count)
		{
			if ((get_time() - philos[i]->last_dinner >
				traits->time_to_die && !philos[i]->is_eating))
			{
				g_table->death = 1;
				death(i);
				return (1);
			}
			i++;
		}
	}
	return (1);
}

int		stop_sim(void)
{
	g_table->death = 1;
	pthread_mutex_lock(&g_table->mx_write);
	return (1);
}

int		catafalque_counter(t_philo **philos, t_traits *traits)
{
	int	i;
	int	full;

	while (!g_table->death)
	{
		i = 0;
		full = 0;
		while (i < g_table->philo_count)
		{
			if ((get_time() - philos[i]->last_dinner >
				traits->time_to_die && !philos[i]->is_eating))
			{
				g_table->death = 1;
				death(i);
				return (1);
			}
			if (philos[i]->eat_count >= traits->max_eating)
				full++;
			if (full >= g_table->philo_count)
				return (stop_sim());
			i++;
		}
	}
	return (1);
}
