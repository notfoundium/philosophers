/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   table.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 22:48:29 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/16 22:48:30 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "table.h"
#include "utils.h"

t_table	*new_table(size_t philo_count)
{
	size_t	i;
	t_table	*table;

	i = 0;
	table = (t_table*)malloc(sizeof(t_table));
	table->forks = (pthread_mutex_t*)ft_calloc(philo_count,
	sizeof(pthread_mutex_t));
	while (i < philo_count)
	{
		pthread_mutex_init(&table->forks[i], NULL);
		i++;
	}
	table->philo_count = philo_count;
	pthread_mutex_init(&table->mx_write, NULL);
	table->death = 0;
	return (table);
}

void	delete_table(t_table *table)
{
	int	i;

	i = 0;
	while (i < table->philo_count)
		pthread_mutex_destroy(&table->forks[i++]);
	free(table->forks);
	free(table);
}
