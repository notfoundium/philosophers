/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   table.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 22:47:51 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/16 22:47:52 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TABLE_H
# define TABLE_H

# include <pthread.h>

typedef struct	s_table
{
	int volatile	philo_count;
	int volatile	death;
	pthread_mutex_t	mx_write;
	pthread_mutex_t	*forks;
}				t_table;

t_table			*g_table;

t_table			*new_table(size_t philo_count);
void			delete_table(t_table *table);

#endif
