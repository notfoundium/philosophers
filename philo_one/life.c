/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   life.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 22:48:12 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/16 22:48:13 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_one.h"
#include "philosopher.h"
#include "utils.h"
#include <unistd.h>
#include <stdio.h>

void	eat(t_philo *philo,
			int pos, int fork_right, int fork_left)
{
	say(pos, MSG_THINK);
	(fork_right < fork_left) ?
	(pthread_mutex_lock(&g_table->forks[fork_right])) :
	(pthread_mutex_lock(&g_table->forks[fork_left]));
	say(pos, MSG_FORK);
	(fork_right < fork_left) ?
	(pthread_mutex_lock(&g_table->forks[fork_left])) :
	(pthread_mutex_lock(&g_table->forks[fork_right]));
	say(philo->pos, MSG_FORK);
	say(philo->pos, MSG_EAT);
	philo->is_eating = 1;
	philo->eat_count++;
	usleep(philo->traits->time_to_eat * 1000);
	philo->last_dinner = get_time();
	philo->is_eating = 0;
	say(pos, MSG_SLEEP);
	(fork_right < fork_left) ?
	(pthread_mutex_unlock(&g_table->forks[fork_left])) :
	(pthread_mutex_unlock(&g_table->forks[fork_right]));
	(fork_right < fork_left) ?
	(pthread_mutex_unlock(&g_table->forks[fork_right]))
	: (pthread_mutex_unlock(&g_table->forks[fork_left]));
	usleep(philo->traits->time_to_sleep * 1000);
}

void	wait_for_death(t_philo *philo)
{
	pthread_mutex_lock(&g_table->forks[philo->fork_right]);
	say(philo->pos, MSG_FORK);
	usleep(philo->traits->time_to_die * 1000);
}

void	*life(void *philo_ref)
{
	t_philo		*philo;
	int			pos;
	int			fork_left;
	int			fork_right;

	philo = (t_philo*)philo_ref;
	pos = philo->pos;
	fork_left = philo->fork_left;
	fork_right = philo->fork_right;
	if (g_table->philo_count < 2)
	{
		wait_for_death(philo);
		return (NULL);
	}
	while (!g_table->death)
		eat(philo, pos, fork_right, fork_left);
	return (NULL);
}
