/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   waiter.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/19 14:55:47 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/19 14:55:48 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_two.h"
#include "utils.h"
#include <semaphore.h>

int		waiter(t_philo **philos, t_traits *traits)
{
	int	i;

	while (!g_death)
	{
		i = 0;
		while (i < g_table->philo_count)
		{
			if ((get_time() - philos[i]->last_dinner >
				traits->time_to_die && !philos[i]->is_eating))
			{
				g_death = 1;
				death(i);
				return (1);
			}
			i++;
		}
	}
	return (1);
}

int		stop_sim(void)
{
	g_death = 1;
	sem_wait(g_sem_wr);
	return (1);
}

int		waiter_counter(t_philo **philos, t_traits *traits)
{
	int	i;
	int	full;

	while (!g_death)
	{
		i = 0;
		full = 0;
		while (i < g_table->philo_count)
		{
			if ((get_time() - philos[i]->last_dinner >
				traits->time_to_die && !philos[i]->is_eating))
			{
				g_death = 1;
				death(i);
				return (1);
			}
			if (philos[i]->eat_count >= traits->max_eating)
				full++;
			if (full >= g_table->philo_count)
				return (stop_sim());
			i++;
		}
	}
	return (1);
}
