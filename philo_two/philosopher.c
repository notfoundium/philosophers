/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosopher.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 22:49:22 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/16 22:49:23 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"
#include "philo_two.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <semaphore.h>

t_philo	*new_philo(int pos, t_traits *traits)
{
	t_philo	*philo;

	philo = (t_philo*)malloc(sizeof(t_philo));
	philo->pos = pos;
	philo->eat_count = 0;
	philo->is_eating = 0;
	philo->traits = traits;
	philo->last_dinner = get_time();
	pthread_create(&philo->thread, NULL, life, philo);
	return (philo);
}

void	delete_philo(t_philo *philo)
{
	pthread_detach(philo->thread);
	free(philo);
}

void	say(int pos, const char *msg)
{
	sem_wait(g_sem_wr);
	if (g_death)
		return ;
	printf("%lld %d %s\n", get_time(), ++pos, msg);
	sem_post(g_sem_wr);
}

void	death(int pos)
{
	sem_wait(g_sem_wr);
	printf("%lld %d died\n", get_time(), ++pos);
}
