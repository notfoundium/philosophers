/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 22:49:10 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/16 22:49:11 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_isdigit(int c)
{
	unsigned char	d;

	d = c;
	return (d >= '0' && d <= '9');
}

int		ft_atoi(const char *nptr)
{
	int number;
	int sign;

	sign = 1;
	number = 0;
	while (*nptr == ' ' || *nptr == '\n' || *nptr == '\t' ||
			*nptr == '\v' || *nptr == '\r' || *nptr == '\f')
		nptr++;
	(*nptr == '-') ? (sign = -1) : (0);
	(*nptr == '+' || *nptr == '-') ? (nptr++) : (0);
	(ft_isdigit(*nptr)) ? (number = sign * (*nptr++ - '0')) : (0);
	while (ft_isdigit(*nptr))
		number = number * 10 + sign * (*nptr++ - '0');
	return (number);
}
