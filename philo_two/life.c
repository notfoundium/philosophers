/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   life.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 22:48:12 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/16 22:48:13 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_two.h"
#include "philosopher.h"
#include "utils.h"
#include <unistd.h>
#include <stdio.h>
#include <semaphore.h>

void	eat(t_philo *philo, int pos, int time_to_eat, int time_to_sleep)
{
	say(pos, MSG_THINK);
	sem_wait(g_forks);
	if (g_death)
		return ;
	say(pos, MSG_FORK);
	say(pos, MSG_FORK);
	say(pos, MSG_EAT);
	philo->eat_count++;
	philo->is_eating = 1;
	usleep(philo->traits->time_to_eat * 1000);
	say(pos, MSG_SLEEP);
	sem_post(g_forks);
	philo->last_dinner = get_time();
	philo->is_eating = 0;
	usleep(philo->traits->time_to_sleep * 1000);
}

void	wait_for_death(t_philo *philo)
{
	usleep(philo->traits->time_to_die * 1000);
}

void	*life(void *philo_ref)
{
	t_philo		*philo;
	int			pos;
	int			time_to_eat;
	int			time_to_sleep;

	philo = (t_philo*)philo_ref;
	pos = philo->pos;
	time_to_eat = philo->traits->time_to_eat;
	time_to_sleep = philo->traits->time_to_sleep;
	if (g_table->philo_count < 2)
	{
		wait_for_death(philo);
		return (NULL);
	}
	while (!g_death)
		eat(philo, pos, time_to_eat, time_to_sleep);
	return (NULL);
}
