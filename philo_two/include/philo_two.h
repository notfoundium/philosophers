/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_two.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/19 14:55:33 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/19 14:55:34 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_TWO_H
# define PHILO_TWO_H

# include "table.h"
# include "philosopher.h"

long long	g_time_start;

void		*life(void *args);
int			waiter(t_philo **philos, t_traits *traits);
int			waiter_counter(t_philo **philos, t_traits *traits);
t_philo		**new_philo_arr(int count, t_traits *traits);
void		delete_philo_arr(t_philo **arr);

#endif
