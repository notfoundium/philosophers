/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   table.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 22:47:51 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/16 22:47:52 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TABLE_H
# define TABLE_H

# include <semaphore.h>

typedef struct	s_table
{
	int volatile	philo_count;
}				t_table;

int volatile	g_death;
t_table			*g_table;
sem_t			*g_forks;
sem_t			*g_sem_wr;

t_table			*new_table(t_table *table, size_t philo_count);
void			delete_table(t_table *table);

#endif
