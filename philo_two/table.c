/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   table.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 22:48:29 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/16 22:48:30 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <stdlib.h>
#include "table.h"
#include "utils.h"

t_table	*new_table(t_table *table, size_t philo_count)
{
	sem_unlink("forks");
	sem_unlink("write");
	g_forks = sem_open("forks", O_CREAT, 0664, philo_count / 2);
	g_sem_wr = sem_open("write", O_CREAT, 0664, 1);
	table->philo_count = philo_count;
	g_death = 0;
	return (table);
}
