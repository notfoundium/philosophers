/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   life.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 22:48:12 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/16 22:48:13 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_three.h"
#include "philosopher.h"
#include "utils.h"
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <semaphore.h>
#include <stdlib.h>
#include <pthread.h>

void	eat(t_philo *philo)
{
	say(philo->pos, MSG_THINK);
	sem_wait(g_forks);
	say(philo->pos, MSG_FORK);
	say(philo->pos, MSG_FORK);
	say(philo->pos, MSG_EAT);
	philo->is_eating = 1;
	if (philo->traits->max_eating > 0 &&
		++philo->eat_count >= philo->traits->max_eating)
	{
		sem_post(g_forks);
		exit(0);
	}
	usleep(philo->traits->time_to_eat * 1000);
	say(philo->pos, MSG_SLEEP);
	philo->is_eating = 0;
	sem_post(g_forks);
	philo->last_dinner = get_time();
	usleep(philo->traits->time_to_sleep * 1000);
}

void	wait_for_death(t_philo *philo)
{
	usleep(philo->traits->time_to_die * 1000);
	death(0);
}

void	*catafalque(void *philo_ref)
{
	t_philo	*philo;

	philo = (t_philo*)philo_ref;
	while (1)
	{
		if (get_time() - philo->last_dinner >=
		philo->traits->time_to_die && !philo->is_eating)
			death(philo->pos);
	}
}

void	life(t_philo *philo)
{
	pthread_t	thread;

	sem_open("write", 0);
	sem_open("forks", 0);
	sem_open("death", 0);
	if (g_table->philo_count < 2)
	{
		wait_for_death(philo);
		exit(0);
	}
	pthread_create(&thread, NULL, catafalque, philo);
	while (1)
		eat(philo);
	exit(0);
}
