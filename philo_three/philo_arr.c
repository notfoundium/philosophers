/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_one.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 22:46:40 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/16 22:46:41 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_three.h"
#include "utils.h"
#include <stdlib.h>

t_philo	**new_philo_arr(int count, t_traits *traits)
{
	t_philo	**philos;
	size_t	pos;

	philos = (t_philo**)ft_calloc(count + 1, sizeof(t_philo));
	pos = 0;
	while (pos < count)
	{
		philos[pos] = new_philo(pos, traits);
		pos++;
	}
	return (philos);
}

void	delete_philo_arr(t_philo **arr)
{
	int	i;

	i = 0;
	while (arr[i])
		delete_philo(arr[i++]);
	free(arr);
}
