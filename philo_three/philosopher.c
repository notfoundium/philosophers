/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosopher.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 22:49:22 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/16 22:49:23 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"
#include "philo_three.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <semaphore.h>
#include <sys/types.h>
#include <signal.h>

t_philo	*new_philo(int pos, t_traits *traits)
{
	t_philo	*philo;

	philo = (t_philo*)malloc(sizeof(t_philo));
	philo->pos = pos;
	philo->eat_count = 0;
	philo->is_eating = 0;
	philo->traits = traits;
	philo->last_dinner = get_time();
	philo->pid = fork();
	if (philo->pid < 0)
		exit(1);
	if (philo->pid == 0)
		life(philo);
	return (philo);
}

void	delete_philo(t_philo *philo)
{
	kill(philo->pid, 9);
	free(philo);
}

void	say(int pos, const char *msg)
{
	sem_wait(g_sem_wr);
	printf("%lld %d %s\n", get_time(), ++pos, msg);
	sem_post(g_sem_wr);
}

void	death(int pos)
{
	sem_wait(g_sem_wr);
	printf("%lld %d died\n", get_time(), ++pos);
	sem_post(g_death);
	exit(0);
}
