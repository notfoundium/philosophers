/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   table.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 22:48:29 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/16 22:48:30 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <stdlib.h>
#include "table.h"
#include "utils.h"

t_table	*new_table(t_table *table, size_t philo_count)
{
	sem_unlink("forks");
	sem_unlink("write");
	sem_unlink("death");
	g_forks = sem_open("forks", O_CREAT, 0664, philo_count / 2);
	g_sem_wr = sem_open("write", O_CREAT, 0664, 1);
	g_death = sem_open("death", O_CREAT, 0664, 0);
	table->philo_count = philo_count;
	return (table);
}

void	delete_table(t_table *table)
{
	sem_close(g_forks);
	sem_close(g_sem_wr);
	sem_close(g_death);
}
