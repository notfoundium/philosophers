/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 22:48:17 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/16 22:48:17 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"
#include "philo_three.h"
#include <unistd.h>
#include <stdlib.h>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h>

int			is_valid(int argc, char **argv)
{
	(void)argv;
	if (argc < 5 || argc > 6)
		return (0);
	if (ft_atoi(argv[1]) < 1 || ft_atoi(argv[1]) > 200)
		return (0);
	if (ft_atoi(argv[2]) < 60)
		return (0);
	if (ft_atoi(argv[3]) < 60)
		return (0);
	if (ft_atoi(argv[4]) < 60)
		return (0);
	if (argc == 6)
		if (ft_atoi(argv[5]) < 1)
			return (0);
	return (1);
}

t_traits	*read_traits(t_traits *traits, int argc, char **argv)
{
	if (!is_valid(argc, argv))
		return (NULL);
	traits->philo_count = ft_atoi(argv[1]);
	traits->time_to_die = ft_atoi(argv[2]) + 1;
	traits->time_to_eat = ft_atoi(argv[3]);
	traits->time_to_sleep = ft_atoi(argv[4]);
	if (argc == 6)
		traits->max_eating = ft_atoi(argv[5]);
	else
		traits->max_eating = 0;
	return (traits);
}

void		*waiter(void *philos_arr)
{
	t_philo	**philos;
	int		i;
	int		stat;
	int		count;
	pid_t	*arr_pid;

	philos = (t_philo**)philos_arr;
	i = 0;
	count = (*philos)->traits->philo_count;
	arr_pid = ft_calloc(count, sizeof(pid_t));
	while (i < count)
	{
		arr_pid[i] = philos[i]->pid;
		i++;
	}
	i = 0;
	while (i < count)
		waitpid(arr_pid[i++], &stat, 0);
	sem_post(g_death);
	free(arr_pid);
	return (NULL);
}

int			main(int argc, char **argv)
{
	t_philo		**philos;
	t_traits	traits;
	int			i;
	t_table		table;
	pthread_t	thread;

	if (!read_traits(&traits, argc, argv))
	{
		write(2, "Invalid arguments\n", 18);
		return (1);
	}
	traits = *read_traits(&traits, argc, argv);
	g_time_start = get_time_start();
	g_table = new_table(&table, traits.philo_count);
	philos = new_philo_arr(traits.philo_count, &traits);
	pthread_create(&thread, NULL, waiter, philos);
	sem_wait(g_death);
	usleep(10000);
	delete_philo_arr(philos);
	pthread_join(thread, NULL);
	delete_table(g_table);
	return (0);
}
