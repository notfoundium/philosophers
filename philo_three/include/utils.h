/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 22:47:59 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/16 22:48:00 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTILS_H
# define UTILS_H

# include <unistd.h>

void		*ft_calloc(size_t nmemb, size_t size);
long long	get_time_start(void);
long long	get_time(void);
void		ft_putnbr(int n);
int			ft_atoi(const char *nptr);
void		ft_putstr(const char *s);

#endif
