/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosopher.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 22:46:59 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/16 22:47:00 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILOSOPHER_H
# define PHILOSOPHER_H

# define MSG_FORK "has taken fork"
# define MSG_EAT "is eating"
# define MSG_SLEEP "is sleeping"
# define MSG_THINK "is thinking"
# define MSG_DIE "died"

typedef struct	s_traits
{
	int			philo_count;
	long long	time_to_die;
	long long	time_to_eat;
	long long	time_to_sleep;
	long long	max_eating;
}				t_traits;

typedef struct	s_philo
{
	pid_t		pid;
	t_traits	*traits;
	int			pos;
	int			is_eating;
	long long	last_dinner;
	int			eat_count;
}				t_philo;

t_philo			*new_philo(int pos, t_traits *traits);
void			delete_philo(t_philo *philo);
void			say(int pos, const char *msg);
void			death(int pos);

#endif
