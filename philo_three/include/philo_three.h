/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_one.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 22:46:51 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/16 22:46:52 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_THREE_H
# define PHILO_THREE_H

# include "table.h"
# include "philosopher.h"
# include <semaphore.h>

long long	g_time_start;

void		life(t_philo *philo);
t_philo		**new_philo_arr(int count, t_traits *traits);
void		delete_philo_arr(t_philo **arr);

#endif
