/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 22:48:56 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/16 22:48:57 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "utils.h"

ssize_t	ft_strlen(const char *str)
{
	ssize_t	len;

	if (!str)
		return (-1);
	len = 0;
	while (str[len])
		len++;
	return (len);
}

void	ft_putchar(int c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int n)
{
	if (n < 0)
	{
		ft_putchar('-');
		(n / (-10) > 0) ? (ft_putnbr(n / (-10))) : ((void)0);
		ft_putchar('0' - n % 10);
	}
	else
	{
		(n / 10 > 0) ? (ft_putnbr(n / 10)) : ((void)0);
		ft_putchar('0' + n % 10);
	}
}

void	ft_putstr(const char *s)
{
	write(1, s, ft_strlen(s));
}
